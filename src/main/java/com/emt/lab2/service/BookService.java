package com.emt.lab2.service;

import com.emt.lab2.model.Book;
import com.emt.lab2.model.dto.BookDto;
import com.emt.lab2.model.enums.Category;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface BookService {
    List<Book> findAll();

    List<Book> loadAll();

    Optional<Book> findById(Long id);
    Optional<Book>findByName(String name);
    Book deleteById(Long id);
    Optional<Book>edit(Long id, String name, Category category,
                       Integer availableCopies, Long authorId);



    Optional<Book> edit(Long id, BookDto bookDto);


    Optional<Book>save(String name, Category category,
                       Integer availableCopies, Long authorId);

    Optional<Book>save(BookDto bookDto);

    Optional<Book> markAsTaken(Long id);

    List<Category> getAllCategories();
}
