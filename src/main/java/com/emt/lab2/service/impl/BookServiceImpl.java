package com.emt.lab2.service.impl;

import com.emt.lab2.model.Author;
import com.emt.lab2.model.Book;
import com.emt.lab2.model.dto.BookDto;
import com.emt.lab2.model.enums.Category;
import com.emt.lab2.model.exceptions.AuthorNotFoundException;
import com.emt.lab2.model.exceptions.BookNotFoundException;
import com.emt.lab2.repository.AuthorRepository;
import com.emt.lab2.repository.BookRepository;
import com.emt.lab2.service.BookService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public BookServiceImpl(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }


    @Override
    public List<Book> findAll() {
        return this.bookRepository.findAll();
    }

    @Override
    public List<Book> loadAll() {
        return null;
    }

    @Override
    public Optional<Book>findById(Long id) {
        return Optional.ofNullable(this.bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(id)));
    }

    @Override
    public Optional<Book> findByName(String name) {
        return this.bookRepository.findByName(name);
    }

    @Override
    public Book deleteById(Long id)
    {
         Book book=this.bookRepository.findById(id).orElseThrow(()->new BookNotFoundException(id));
         this.bookRepository.delete(book);
         return book;
    }

    @Override
    @Transactional
    public Optional<Book> edit(Long id, String name, Category category, Integer availableCopies, Long authorId) {
        Book book = this.bookRepository.findById(id).orElseThrow(()->new BookNotFoundException(id));
        book.setName(name);
        book.setCategory(category);
        book.setAvailableCopies(availableCopies);
        Author author=this.authorRepository.findById(authorId).orElseThrow(()-> new AuthorNotFoundException(authorId));
        book.setAuthor(author);
        this.bookRepository.save(book);
        return Optional.of(book);
    }

    @Override
    public Optional<Book> edit(Long id, BookDto bookDto) {
        Author author=this.authorRepository.findById(bookDto.getAuthorId()).orElseThrow(()->new AuthorNotFoundException(bookDto.getAuthorId()));
     //   this.bookRepository.deleteByName(bookDto.getName());
        //Book book=new Book(bookDto.getName(), bookDto.getCategory(), bookDto.getAvailableCopies(), author);
        Book book = this.bookRepository.findById(id).orElseThrow(()->new BookNotFoundException(id));
        book.setName(bookDto.getName());
        book.setCategory(bookDto.getCategory());
        book.setAvailableCopies(bookDto.getAvailableCopies());
        book.setAuthor(author);
        this.bookRepository.save(book);
        return Optional.of(book);
    }

    @Override
    @Transactional
    public Optional<Book>save(String name, Category category,
                              Integer availableCopies, Long authorId)
    {
        Author author=this.authorRepository.findById(authorId).orElseThrow(()->new AuthorNotFoundException(authorId));
        this.bookRepository.deleteByName(name);
        Book book=new Book(name,category,availableCopies,author);
        this.bookRepository.save(book);
        return Optional.of(book);
    }

    @Override
    public Optional<Book>save(BookDto bookDto)
    {
        Author author=this.authorRepository.findById((bookDto.getAuthorId())).orElseThrow(()->new AuthorNotFoundException(bookDto.getAuthorId()));
        this.bookRepository.deleteByName(bookDto.getName());
        Book book=new Book(bookDto.getName(), bookDto.getCategory(), bookDto.getAvailableCopies(), author);
        this.bookRepository.save(book);
        return Optional.of(book);
    }

    @Override
    public Optional<Book> markAsTaken(Long id)
    {
        Book book=this.bookRepository.findById(id).orElseThrow(()->new BookNotFoundException(id));
        book.setAvailableCopies(book.getAvailableCopies()-1);
        this.bookRepository.save(book);
        return Optional.of(book);
    }

    @Override
    public List<Category> getAllCategories()
    {
        return List.of(Category.values());
    }
}
