package com.emt.lab2.model;

import com.emt.lab2.model.enums.Category;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Category category;
    @ManyToOne
    private Author author;
    private Integer availableCopies;

    public Book(Long id, String name, Category category, Author author, Integer availableCopies) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.author = author;
        this.availableCopies = availableCopies;
    }

    public Book() {
    }

    public Book(String name, Category category, Integer availableCopies, Author author) {
        this.name=name;
        this.category=category;
        this.availableCopies=availableCopies;
        this.author=author;
    }
}
