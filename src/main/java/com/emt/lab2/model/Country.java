package com.emt.lab2.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long countryId;
    private String countryName;
    private String continent;

    public Country(Long countryId, String countryName, String continent) {
        this.countryId = countryId;
        this.countryName = countryName;
        this.continent = continent;
    }

    public Country() {
    }
}
