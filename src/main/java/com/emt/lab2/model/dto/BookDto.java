package com.emt.lab2.model.dto;

import com.emt.lab2.model.Author;
import com.emt.lab2.model.enums.Category;
import lombok.Data;

@Data
public class BookDto {
    private String name;
    private Category category;
    private Integer availableCopies;
    private Long authorId;

    public BookDto(String name, Category category, Integer availableCopies, Long authorId) {
        this.name = name;
        this.category = category;
        this.availableCopies = availableCopies;
        this.authorId = authorId;
    }

    public BookDto() {
    }
}
