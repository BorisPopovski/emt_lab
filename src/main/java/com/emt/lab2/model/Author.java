package com.emt.lab2.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long authorId;
    private String authorName;
    private String authorSurname;
    @ManyToOne
    private Country authorCountry;

    public Author(Long authorId, String authorName, String authorSurname, Country authorCountry) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.authorSurname = authorSurname;
        this.authorCountry = authorCountry;
    }

    public Author() {
    }
}
