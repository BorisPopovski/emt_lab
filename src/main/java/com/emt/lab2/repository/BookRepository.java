package com.emt.lab2.repository;

import com.emt.lab2.model.Book;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book,Long> {

//    @EntityGraph(type= EntityGraph.EntityGraphType.FETCH,attributePaths = {"author"})
//    @Query("SELECT b from Book b")
//    List<Book> findAll();
//
//    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD,attributePaths = {"author"})
//    @Query("SELECT b from Book b")
//    List<Book> loadAll();


    Optional<Book> findById(Long id);

    Optional<Book> findByName(String name);

    void deleteByName (String name);
}
