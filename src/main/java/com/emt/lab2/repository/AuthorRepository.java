package com.emt.lab2.repository;

import com.emt.lab2.model.Author;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<Author,Long> {
    Optional<Author> findById(Long id);

    @EntityGraph(type= EntityGraph.EntityGraphType.FETCH,attributePaths = {"country"})
    @Query("SELECT a from Author a")
    List<Author>findAll();
}
